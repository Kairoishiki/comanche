#pragma once

#include "entities/object.hpp"

class Camera {
public:
    Camera() = default;
    Camera(float fov, float d);
    Camera(Object* obj);
    Camera(Object* obj, Object* defaultObj);

    /**
     * \brief attach the camera to a given object
     * \param obj the object to follow
    */
    void attach(Object* obj);

    /**
     * \brief sets the default object to follow
     * \param defaultObj the default object
    */
    void setDefault(Object* defaultObj);

    /**
     * \brief get the focal angle of the camera
     * \return the focal angle
    */
    float fov() const;

    /**
     * \brief get the tilt angle of the camera
     * \return the tilt angle
    */
    float angleZ() const;

    /**
     * \brief get the view distance of the camera
     * \return the view distance
    */
    float d() const;

    /**
     * \brief get the view vector of the camera
     * \return the view vector
    */
    glm::vec2 view() const;

    /**
     * \brief get the position in the 3D space of the camera
     * \return the positition
    */
    glm::vec3 position() const;
    Object* getObj() const;

    /**
     * \brief increases the view distance of the camera
     * \param n increase by n
    */
    void incFovD(float n);
private:
    float m_fov;
    float m_d;
    Object* m_object;
    Object* m_defaultObject;
};
