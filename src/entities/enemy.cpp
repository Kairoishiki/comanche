#include "enemy.hpp"
#include "object.hpp"
#include "../../lib/glm/glm.hpp"
#include "../../lib/glm/gtx/rotate_vector.hpp" // glm::rotate
#include "../map.hpp"
#include "helicopter.hpp"
#include <iostream>

Enemy::Enemy(glm::vec3 position, Map& map) {
    //Object(position, glm::vec3(50.f, 50.f, 0.f), 0.f);
    Object();
    m_position = position;
    m_distance = 0.f;
    //m_counter = 0;
    //m_veritalDiff = 0;
    m_visible = false;

    m_inMap = std::vector<bool>(map.size());
    std::fill(m_inMap.begin(), m_inMap.end(), false);
    //m_inMap[size] = {false};

    int x = int(m_position.x) % map.width();
    int y = int(m_position.y) % map.height();

    x = (x + map.width()) % map.width();
    y = (y + map.height()) % map.height();
    m_inMap[x * map.width() + y] = true;

    m_position.z = map.getHeightAt(m_position.x, -m_position.y);
}

void Enemy::setVisible(bool set) {
    m_visible = set;
}

auto Enemy::inArray(int x, int y, int width, int height) -> bool {
    x %= width;
    y %= height;
    x = (x + width) % width;
    y = (y + height) % height;
    return m_inMap[x * width + y];
}

auto Enemy::getDistance() -> float {
    return m_distance;
}

/*
  nColonnes: nombre de colonne
  std::fill(heights.begin(), heights.end(), window.height()): remplie le tableau height par la hauteur de la fenetre
 */
void Enemy::updatePosition(Helicopter& player, Map& map) {
    //m_distance = glm::distance(player.position(), m_position);

    m_distance = glm::distance(glm::vec2(player.position().x, player.position().y),
			       glm::vec2(m_position.x, -m_position.y));

    //m_position.x += 0.5;

    std::fill(m_inMap.begin(), m_inMap.end(), false);
    int x = int(m_position.x) % map.width();
    int y = int(m_position.y) % map.height();
    x = (x + map.width()) % map.width();
    y = (y + map.height()) % map.height();
    m_inMap[x * map.width() + y] = true;
    m_position.z = map.getHeightAt(m_position.x, -m_position.y) + 100;
}

void Enemy::setCamPosition(const glm::vec2 drawVector, const glm::vec2 horizonPoint, const float renderedY) {
    m_drawVector = drawVector;
    m_horizonPoint = horizonPoint;
    m_renderedY = renderedY;
}

//void Enemy::render(SDL::Renderer& renderer, const glm::vec2 drawVector, const glm::vec2 horizonPoint, const float renderedY) {
void Enemy::render(SDL::Renderer& renderer) {
    if (m_visible) {
	const auto hauteur = m_horizonPoint + m_drawVector * m_renderedY;
    const SDL::Rect dst{hauteur.x, hauteur.y,
			       pow(100, 2) / m_distance,
			       pow(100, 2) / m_distance};
    static auto texture = SDL::Texture("res/tank_front.png", renderer);
    texture.display(texture.getRect(), dst);
    /*
	renderer.setColor({200, 100, 100, 255});
	renderer.drawRectangle(hauteur.x, hauteur.y,
			       pow(100, 2) / m_distance,
			       pow(100, 2) / m_distance);
    */
    // std::cout << m_position.x << " " << m_position.y << std::endl;
	// std::cout << "trouvé" << m_distance << std::endl;
    }
    //m_counter++;
    //if (m_counter > 5) {
    //	setVisible(false);
    //}
    setVisible(false);
}
