#include "object.hpp"

Object::Object(glm::vec3 position, glm::vec3 direction, float angleZ) {
    m_position = position;
    m_direction = direction;
    m_angleZ = angleZ;
}

glm::vec3 Object::position() const {
    return m_position;
}

glm::vec3 Object::direction() const {
    return m_direction;
}

float Object::angleZ() const {
    return m_angleZ;
}