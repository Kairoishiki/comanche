#include "helicopter.hpp"
#include "../game.hpp"
#include "../../lib/glm/glm.hpp"
#include "../../lib/glm/geometric.hpp"
#include "../../lib/glm/trigonometric.hpp"
#include "../../lib/glm/gtx/rotate_vector.hpp"
#include <iostream>
#include <sstream>

Helicopter::Helicopter(glm::vec3 position) :
    Object(position, glm::vec3(1.f, 0.f, 0.f), 0.f),
    m_isTurning(false)
{
    
}

void Helicopter::update() {
    constexpr auto smoothFactor = 5.f;
    // if the helicopter isn't turning, we set its tilt angle back to 0
    if(!m_isTurning) {
        float diff = -m_angleZ;
        m_angleZ += (diff / smoothFactor);
    } else {
        m_isTurning = false;
    }
}

void Helicopter::forward(float n) {
    m_position += n * m_direction;
}

void Helicopter::verticalMove(float n) {
    m_position += n * glm::vec3(0, 0, 1);
}

void Helicopter::turn(float angle) {
    // rotate the direction vector
    m_direction = glm::rotateZ(m_direction, angle);
    // maximum tilt angle and smooth factor
    constexpr auto maxTiltAngle = glm::quarter_pi<float>() / 2.f;
    constexpr auto smoothFactor = 20.f;
    // adjust tilt angle
    if(angle > 0) {
        float diff = maxTiltAngle - m_angleZ;
        float sign = diff < 0 ? -1.f : 1.f;
        m_angleZ += sign * (diff / smoothFactor);
    } else {
        float diff = - maxTiltAngle - m_angleZ;
        float sign = diff < 0 ? -1.f : 1.f;
        m_angleZ -= sign * (diff / smoothFactor);
    }
    // indicate that the helicopter is currently turning
    m_isTurning = true;
}

template<typename T>
std::string toString(T value) {
    std::stringstream ss;
    std::string result;
    ss << value;
    ss >> result;
    return result;
}

void Helicopter::render(Game* ctx) {
    auto& renderer = ctx->renderer();
    const auto winw = ctx->window().width();
    const auto winh = ctx->window().height();
    // various constants
    constexpr auto f = 1;
    constexpr auto lineSize = 200.f;
    constexpr auto circleRadius = 15.f;
    constexpr auto perpLineSize = 20.f;
    constexpr auto textAltitudeOffset = 20;
    constexpr auto step = 25;
    constexpr auto onScreenStep = 60;
    const auto groundAltitude = ctx->map().getHeightAt(m_position.x, -m_position.y);
    // various constants
    const auto altitude = (m_position.z - groundAltitude) * f;
    const auto viewBegin = winh; // 2 * winh / 3;
    const auto altitudeRenderPos = viewBegin / 2;
    const glm::vec2 tiltVector{ glm::cos(m_angleZ), -glm::sin(m_angleZ) };
    const auto perpendicular = glm::rotate(tiltVector, - glm::half_pi<float>());
    const glm::vec2 pilotScreenMiddle{winw / 2.f, viewBegin / 2.f};
    // render points for altitude
    const auto tiltLineBeginLeft = - tiltVector * (lineSize / 2) + pilotScreenMiddle;
    const auto tiltLineEndLeft = - tiltVector * circleRadius + pilotScreenMiddle;
    const auto tiltLineBeginRight = tiltVector * circleRadius + pilotScreenMiddle;
    const auto tiltLineEndRight = tiltVector * (lineSize / 2) + pilotScreenMiddle;
    // second line
    const auto perpTiltLineBegin = perpendicular * circleRadius + pilotScreenMiddle;
    const auto perpTiltLineEnd = perpendicular * (perpLineSize + circleRadius) + pilotScreenMiddle;
    auto& fontAltitude = ctx->fontManager()[{"arial", 15}];
    // render cockpit base
    renderer.setColor(SDL::Color::BLACK);
    renderer.drawRectangle(0, viewBegin, winw, winh);
    // render altitude info
    renderer.setColor(SDL::Color::GREEN);
    renderer.drawLine(tiltLineBeginLeft.x, tiltLineBeginLeft.y, tiltLineEndLeft.x, tiltLineEndLeft.y);
    renderer.drawLine(tiltLineBeginRight.x, tiltLineBeginRight.y, tiltLineEndRight.x, tiltLineEndRight.y);
    renderer.drawCircle(pilotScreenMiddle.x, pilotScreenMiddle.y, circleRadius);
    renderer.drawLine(perpTiltLineBegin.x, perpTiltLineBegin.y, perpTiltLineEnd.x, perpTiltLineEnd.y);
    renderer.drawLine(perpTiltLineBegin.x, perpTiltLineBegin.y, perpTiltLineEnd.x, perpTiltLineEnd.y);

    for(int i = -2 ; i <= 2 ; i += 1) {
        const auto round = (int(altitude) / step) * step;
        const auto diff = int(altitude) % step;
        const auto onScreenDiff = (onScreenStep * diff) / step;
        const auto textAltitude = toString(round + - i * step);
        const auto [sax, say] = fontAltitude.sizeText(textAltitude);
        renderer.renderText(
            textAltitude,
            fontAltitude,
            winw / 2 - lineSize / 2.f - sax - textAltitudeOffset, altitudeRenderPos - say / 2 + i * onScreenStep + onScreenDiff
        );
        renderer.renderText(
            textAltitude,
            fontAltitude,
            winw / 2 + lineSize / 2.f + textAltitudeOffset, altitudeRenderPos - say / 2 + i * onScreenStep + onScreenDiff
        );
    }

}