#pragma once

#include "object.hpp"
#include "helicopter.hpp"
#include "../sdlwrapper/sdlwrapper.hpp"

class Map;

class Helicopter;

class Enemy : public Object {
public:
    Enemy() = default;
    Enemy(glm::vec3 position, Map& map);
    Enemy(const Enemy&) = default;

    /**
     * \brief check if the enemy is in the field of view of the camera
     * \param x coordinate in the field of view
     * \param y coordinate in the field of view
     * \param width of the map
     * \param height of the map
     * \return if the enemy is in the field of view
     */
    bool inArray(int x, int y, int width, int height);

    /**
     * \brief update the position of the enemy in a 3D space
     * \param Helicopter the player
     * \param Map the map
     */
    void updatePosition(Helicopter& player, Map& map);

    /**
     * \brief draw the enemy on the screen
     * \param SDL::Renderer the render from the SDL librarie
     */
    void render(SDL::Renderer& renderer);

    /**
     * \brief show the distance between the player and the enemy
     * \return the distance between the player and the enemy
     */
    float getDistance();

    /**
     * \brief update the visibility of the enemy in the screen
     */
    void setVisible(bool set);

    /**
     * \brief update the camera position for knowing where to draw
     * \param glm::vec2 the Vector
     * \param glm::vec2 the horizon point
     * \param const float the renderedY
     */
    void setCamPosition(const glm::vec2 drawVector, const glm::vec2 HorizonPoint, const float renderedY);
private:
    float m_distance;
    std::vector<bool> m_inMap;
    bool m_visible;
    glm::vec2 m_drawVector, m_horizonPoint;
    float m_renderedY;
};
