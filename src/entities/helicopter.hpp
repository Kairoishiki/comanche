#pragma once

#include "object.hpp"
#include "../sdlwrapper/sdlwrapper.hpp"
#include "../map.hpp"

class Game;

class Helicopter : public Object {
public:
    Helicopter() = default;
    Helicopter(glm::vec3 position);
    Helicopter(const Helicopter&) = default;

    /**
     * \brief moves the helicopter forward
     * \param n
    */
    void forward(float n);

    /**
     * \brief moves the helicopter upwards
     * \param n
    */
    void verticalMove(float n);

    /**
     * \brief turns the helicopter
     * \param angle
    */
    void turn(float angle);

    /**
     * \brief update the helicopter info
    */
    void update();

    /**
     * \brief renders the helicopter given the context of the game
     * \param ctw the context of the game, i.e the game itself
    */
    void render(Game* ctx);
private:
    bool m_isTurning;
};
