#pragma once

#include "../../lib/glm/glm.hpp"

class Object {
public:
    Object() = default;
    Object(glm::vec3 position, glm::vec3 direction, float angleZ);

    /**
     * \brief getter for the position of the object
     * \return the 3D position of the object
    */
    glm::vec3 position() const;

    /**
     * \brief getter for the direction of the object
     * \return the direction on the object
    */
    glm::vec3 direction() const;

    /**
     * \brief getter for the tilt angle of the object
     * \return the tilt angle of the object
    */
    float angleZ() const;
protected:
    glm::vec3 m_position;
    glm::vec3 m_direction;
    float m_angleZ;
};