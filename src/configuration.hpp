#pragma once

#include <string>
#include <vector>
#include <memory>
#include <utility>


namespace config {

    /**
     * \struct MapProperties
     * \brief represents the properties of a map in the game
    */
    struct MapProperties {
        std::string mapName; /** name of the map */
        std::string texturePath; /** path to the texture image */
        std::string heightMapPath; /** path to the height map */
        std::vector<std::pair<float, float>> enemyPositions; /** positions of the enemies */
    };

    /**
     * \struct Configuration
     * \brief represents the configuration of the game
    */
    struct Configuration {
        std::vector<MapProperties> availableMaps;
    };

    /**
     * \brief Parse a configuration file
     * \param path the path to the configuration file
     * \return the configuration
    */
    Configuration parseConfig(const std::string& configPath);

}