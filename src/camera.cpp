#include "camera.hpp"
#include "../lib/glm/gtx/vector_angle.hpp"
#include <iostream>

Camera::Camera(float fov, float d) {
    m_fov = fov;
    m_d = d;
}

Camera::Camera(Object* obj) {
    m_object = obj;
    m_defaultObject = obj;
}

Camera::Camera(Object* obj, Object* defaultObj) {
    m_object = obj;
    m_defaultObject = defaultObj;
}

void Camera::attach(Object* obj) {
    // std::cout << obj << "oki\n";
    m_object = obj;
}

void Camera::setDefault(Object* obj) {
    m_defaultObject = obj;
}

float Camera::fov() const {
    return m_fov;
}

void Camera::incFovD(float n) {
    m_d += n;
}

glm::vec2 Camera::view() const {
    auto direction = m_object->direction();
    return glm::normalize(glm::vec2(direction.x, -direction.y));
}

float Camera::angleZ() const {
    return m_object->angleZ();
}

glm::vec3 Camera::position() const {
    return m_object->position();
}

float Camera::d() const {
    return m_d;
}

Object* Camera::getObj() const {
    return m_object;
}