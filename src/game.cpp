#include "game.hpp"
#include "sdlwrapper/sdlwrapper.hpp"
#include "../lib/glm/glm.hpp"
#include "../lib/glm/geometric.hpp"
#include "../lib/glm/trigonometric.hpp"
#include "../lib/glm/gtx/rotate_vector.hpp"
#include "map.hpp"
#include "entities/enemy.hpp"
#include <iostream>
#include <sstream>

constexpr auto testFontFilename = "res/fonts/arial.ttf";
constexpr auto FPS = 60;
constexpr auto Max_Keybind = 5;

Map& Game::map() {
    return m_map;
}
SDL::Window& Game::window() {
    return m_window;
}
SDL::Renderer& Game::renderer() {
    return m_renderer;
}
TTF::FontManager& Game::fontManager() {
    return m_fontManager;
}

//#define MAX_KEYBINDS 4
static SDL_Scancode input[Max_Keybind] = {
    (SDL_Scancode)0,
    (SDL_Scancode)0,
    (SDL_Scancode)0,
    (SDL_Scancode)0,
    (SDL_Scancode)0
};

auto Game::ProcessKeyDown(SDL_KeyboardEvent *ev) -> void{
    SDL_Scancode code = ev->keysym.scancode;
    switch(code) {
    case SDL_SCANCODE_W:
    case SDL_SCANCODE_S:
	input[0] = code;
	break;
    case SDL_SCANCODE_A:
    case SDL_SCANCODE_D:
	input[1] = code;
	break;
    case SDL_SCANCODE_E:
    case SDL_SCANCODE_Q:
	input[2] = code;
	break;
    case SDL_SCANCODE_R:
    case SDL_SCANCODE_F:
	input[3] = code;
	break;
    case SDL_SCANCODE_O:
    case SDL_SCANCODE_P:
    input[4] = code;
    break;
    case SDL_SCANCODE_ESCAPE:
    case SDL_SCANCODE_CAPSLOCK:
	m_running = false;
    default:
	break;
    }
    
}

auto Game::ProcessKeyUp(SDL_KeyboardEvent *ev) -> void {
    SDL_Scancode code = ev->keysym.scancode;
    switch(code) {
    case SDL_SCANCODE_W:
    case SDL_SCANCODE_S:
	if (input[0] == code) input[0] = (SDL_Scancode)0;
	break;
    case SDL_SCANCODE_A:
    case SDL_SCANCODE_D:
	if (input[1] == code) input[1] = (SDL_Scancode)0;
	break;
    case SDL_SCANCODE_E:
    case SDL_SCANCODE_Q:
	if (input[2] == code) input[2] = (SDL_Scancode)0;
	break;
    case SDL_SCANCODE_R:
    case SDL_SCANCODE_F:
	if (input[3] == code) input[3] = (SDL_Scancode)0;
	break;
    case SDL_SCANCODE_O:
    case SDL_SCANCODE_P:
	if (input[4] == code) input[4] = (SDL_Scancode)0;
	break;
    default: break;
    }
}

auto Game::ProcessSDLEvents() -> void {
    SDL_Event ev;
    //char* tmpsymptr, typesym;
    while(SDL_PollEvent(&ev)) {
	switch(ev.type) {
	case SDL_QUIT:
	    m_running = false;
	    break;
	case SDL_KEYDOWN:
        switch(m_frame) {
            case Frame::game_frame:
                ProcessKeyDown(&ev.key);
                break;
            case Frame::menu_frame:
                m_menu.processKeyDown(&ev.key);
                break;
        }
	    break;
	case SDL_KEYUP:
	    switch(m_frame) {
            case Frame::game_frame:
                ProcessKeyUp(&ev.key);
                break;
            default: break;
        }
	    break;
	}
    }
}

auto Game::UpdateInput() -> void {
    // A/E (Up/Down)
    if (input[2]) {
	    // m_cam.Position.z += (input[2] == SDL_SCANCODE_E ? 4.0f : -4.0f);
        float direction = input[2] == SDL_SCANCODE_E ? 1.0f : -1.0f;
        m_helicopter.verticalMove(direction * 4.f);
    }

    
    // R/F (Increase/Decrease Distance)
    if (input[3]) {
	    // m_cam.d += (input[3] == SDL_SCANCODE_R ? 40.0f : -40.0f);
        float n = input[3] == SDL_SCANCODE_R ? 40.0f : -40.0f;
        m_cam.incFovD(n);
    }

    /*
    // O/P (Increase/Decrease Z-angle)
    if(input[4]) {
        m_cam.angleZ += (input[4] == SDL_SCANCODE_O ? 0.1f : -0.1f);
    }
    */

    
    // Q/D (Left/Right)
    if (input[1]) {
	    float direction = (input[1] == SDL_SCANCODE_A ? -1.0f : 1.0f);
        m_helicopter.turn(- 0.1f * direction);
    }

    // Z/S (Forward/Backward)
    if (input[0]) {
	    float direction = input[0] == SDL_SCANCODE_W ? 1.0f : -1.0f;
        m_helicopter.forward(direction * 4);
    }
    m_helicopter.update();

    // debug enemy position
    //for (auto enemy : m_enemies) {
    m_enemy.updatePosition(m_helicopter, m_map);
	//}
    //m_enemy.updatePosition(m_helicopter, m_map);
}

auto Game::run() -> void {
    Uint32 FPS = 30;
    //Camera camera {3.14 / 6., 3.14 / 5., 400, glm::vec3(0, 0, 0)};
    // std::cout << "run " << m_cam.getObj() << std::endl;
    while(m_running) {

        // Traitement des évènements SDL
        ProcessSDLEvents();

        if(m_frame == Frame::game_frame) {
            // Modifie la position de la caméra and helicopter
            UpdateInput();
            m_renderer.setColor(SDL::Color::BLUESKY);
            m_renderer.clear();
            m_map.render(m_window, m_renderer, m_cam, m_enemy);
            m_enemy.render(m_renderer);
            m_helicopter.render(this);
        } else {
            m_menu.render();
        }
        m_renderer.renderPresent();
	    SDL::waitFPS(FPS);
    }
}

void Game::loadGame(const config::MapProperties& map) {
    m_map = Map(map.texturePath, map.heightMapPath);
    m_helicopter = Helicopter(glm::vec3(0, 0, 255));
    const auto [dx, dy] = map.enemyPositions[0];
    m_enemy = Enemy(glm::vec3(dx * m_map.width(), dy * m_map.height(), 127.5), m_map);
    //m_enemies = std::vector<Enemy>();
    //for (auto enemy : enemies) {
    //	int x = enemy.first;
    //	int y = enemy.second;
    //	std::cout << x << " " << y << std::endl;
    //	m_enemies.push_back(Enemy(glm::vec3(x, y, 255), m_map));
    //}
    // m_cam = {3.14 / 2., 0, 0, 800, glm::vec3(0, 0, 255)};
    m_cam = Camera(glm::half_pi<float>(), 800);
    m_cam.attach(&m_helicopter);
    m_frame = Frame::game_frame;
}

Game::Game(const std::string& cMap, const std::string& hMap, config::Configuration& config):
    m_window("comanche", 10, 10, 800, 800, SDL_WINDOW_SHOWN),
    m_renderer(m_window, -1, SDL_RENDERER_ACCELERATED),
    m_configuration(std::ref(config))
{
    m_fontManager.openFont(testFontFilename, "arial", 50);
    m_fontManager.openFont(testFontFilename, "arial", 40);
    m_fontManager.openFont(testFontFilename, "arial", 35);
    m_fontManager.openFont(testFontFilename, "arial", 30);
    m_fontManager.openFont(testFontFilename, "arial", 20);
    m_fontManager.openFont(testFontFilename, "arial", 15);

    m_running = true;
    m_menu = Menu(this, &config);
    m_frame = Frame::menu_frame;
}
