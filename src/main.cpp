#include "sdlwrapper/sdlwrapper.hpp"
#include "game.hpp"
#include "configuration.hpp"
#include <algorithm>
#include <iostream>

//auto getCmdOption(char **begin, char **end, const std::string &option) -> char* {
//    char **itr = std::find(begin, end, option);
//    if (itr != end && ++itr != end) {
//	return *itr;
//    }
//    return 0;
//}

auto main(int argc, char **argv) -> int {
    //char *colormap = getCmdOption(argv, argv + argc, "-c");
    //char *heightmap = getCmdOption(argv, argv + argc, "-h");
    //if (colormap == NULL || heightmap == NULL) {
    //	std::cerr << "Usage:\n"
    //		  << "comanche [-h <heightmap>] [-c <colormap>]" << std::endl;
    //	exit(1);
    //}

    //std::cout << "file for colormap: " << colormap
    //	      << "\nfile for heightmap: " << heightmap << std::endl;

    std::cout << "> parsing config...";
    auto config = config::parseConfig("config.toml");
    std::cout << "> done\n";

    for(const auto& mapInfo : config.availableMaps) {
        std::cout << mapInfo.mapName << " : [";
        for(const auto& position : mapInfo.enemyPositions) {
            std::cout << "(" << position.first << ", " << position.second << ")";
        }
        std::cout << "]\n";
    }

    auto map = config.availableMaps[1];
    auto enemy = config.availableMaps[1].enemyPositions;
    std::cout << "! map '" << map.mapName << "' chosen\n";

    SDL::init();
    Game game(map.texturePath, map.heightMapPath, config);
    game.run();
    SDL::quit();
}
