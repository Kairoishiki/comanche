#pragma once

#include <functional>
#include <vector>
#include "configuration.hpp"
#include "sdlwrapper/sdlwrapper.hpp"

class Game;

class Menu {
public:
    Menu() = default;
    Menu(Game* game, const config::Configuration* config);

    /**
     * \brief process the event of a key down
     * \param event the key event
    */
    void processKeyDown(SDL_KeyboardEvent* event);

    /**
     * \brief renders the menu on screen
    */
    void render();
private:
    void handleChangeSelection(SDL_Scancode code);
    const config::Configuration* m_configuration;
    Game* m_game;
    int m_selected;
    std::vector<SDL::Texture> m_mapImages;
};