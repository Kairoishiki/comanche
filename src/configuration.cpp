#include "configuration.hpp"
#include <fstream>
#include <iostream>
#include <sstream>
#include <map>
#include <memory>

namespace config {

namespace {
    /**
     * \struct Token
     * \brief Represent a token
    */
    struct Token {
        enum token_kind_t {
            LBRACKET,
            RBRACKET,
            LPAREN,
            RPAREN,
            EQUAL,
            MINUS,
            COMA,
            STRING,
            NUMBER,
            IDENTIFIER,
            EOL
        } kind; /** the kind of the token */
        std::string text; /** the string */
        double number; /** the number */
    };

    /**
     * \brief Read a file and return a stream of tokens
     * \param file the file stream
     * \return a stream of tokens
    */
    std::vector<Token> tokenize(std::ifstream& file) {
        std::vector<Token> result;
        char current;
        while(!file.eof()) {
            file.get(current);
            if(current == '[') {
                result.push_back(Token{ Token::LBRACKET, "[", 0});
            }
            else if(current == ']') {
                result.push_back(Token{ Token::RBRACKET, "]", 0});
            }
            else if(current == '(') {
                result.push_back(Token{ Token::LPAREN, "(", 0});
            }
            else if(current == ')') {
                result.push_back(Token{ Token::RPAREN, ")", 0});
            }
            else if(current == ',') {
                result.push_back(Token{ Token::COMA, ",", 0});
            }
            else if(current == '-') {
                result.push_back(Token{ Token::MINUS, "-", 0});
            }
            else if(current == '=') {
                result.push_back(Token{ Token::EQUAL, "=", 0});
            }
            else if(current == '\n') {
                result.push_back(Token{ Token::EOL, "EOL", 0});
            }
            else if(std::isalpha(current)) {
                std::string identifier;
                while(std::isalpha(current)) {
                    identifier.push_back(current);
                    file.get(current);
                }
                file.unget();
                result.push_back(Token { Token::IDENTIFIER, identifier, 0 });
            } else if(std::isdigit(current)) {
                std::string number;
                bool coma = false;
                while(std::isdigit(current)) {
                    number.push_back(current);
                    file.get(current);
                    if(current == '.') {
                        if(!coma) {
                            number.push_back(current);
                            file.get(current);
                            coma = true;
                        } else {
                            break;
                        }
                    }
                }
                file.unget();
                result.push_back(Token{Token::NUMBER, number, std::stod(number)});
            } else if(current == '"') {
                std::string str;
                file.get(current);
                while(current != '"') {
                    str.push_back(current);
                    file.get(current);
                }
                file.get(current);
                result.push_back(Token{Token::STRING, str, 0});
            }
        }
        return result;
    }

    /**
     * \class Entry
     * \brief Base class representing an entry in the configuration file
    */
    class Entry {
    public:
        virtual ~Entry() = default;
        virtual std::string as_string() {}
        virtual float as_float() {}
        virtual std::vector<std::pair<float, float>> as_positions() {}
    };

    /**
     * \class StringEntry
     * \brief Represents a textual entry
    */
    class StringEntry : public Entry {
    public:
        StringEntry(StringEntry&&) = default;
        StringEntry(const std::string& value) : m_value(value) {}
        std::string as_string() override { return m_value; }
    private:
        std::string m_value;
    };

    /**
     * \class NumberEntry
     * \brief Represents a numeric entry
    */
    class NumberEntry : public Entry {
    public:
        NumberEntry(NumberEntry&&) = default;
        NumberEntry(float value) : m_value(value) {}
        float as_float() override { return m_value; }
    private:
        float m_value;
    };

    /**
     * \class PositionsEntry
     * \brief Represents an array of tuples
    */
    class PositionsEntry : public Entry {
    public:
        PositionsEntry(PositionsEntry&&) = default;
        PositionsEntry(const std::vector<std::pair<float, float>>& value) : m_value(value) {}
        std::vector<std::pair<float, float>> as_positions() override { return m_value; }
    private:
        std::vector<std::pair<float, float>> m_value;
    };

    /**
     * \class ConfigurationParser
     * \brief Parser for the configuration file
    */
    class ConfigurationParser {
    public:
        ConfigurationParser(const std::vector<Token>& tokens):
            m_tokens(tokens),
            m_pos(0)
        {}
        /**
         * \brief parse the stream of tokens
         * \return the configuration read
        */
        Configuration parse() {
            std::vector<MapProperties> availableMaps;
            ignoreLines();
            while(current().kind == Token::LBRACKET) {
                auto entry = parseMapProperties();
                availableMaps.push_back(entry);
                ignoreLines();
            }
            return {availableMaps};
        }
    private:
        int m_pos;
        std::vector<Token> m_tokens;

        /**
         * \brief tell if the end of stream is reached
        */
        bool end() const {
            return m_pos >= m_tokens.size();
        }

        /**
         * \brief return the current token
        */
        const Token& current() const {
            return m_tokens[m_pos];
        }

        /**
         * \brief ignore the following EOL tokens
        */
        void ignoreLines() {
            while(!end() && current().kind == Token::EOL) {
                next();
            }
        }

        /**
         * \brief go to the next token
        */
        void next() {
            m_pos += 1;
        }

        /**
         * \brief expect the current token to be of kind, return the current token then go to the next token
        */
        Token nextExpect(Token::token_kind_t kind) {
            if(end() || current().kind != kind) {
                throw "Exception";
            }
            auto result = current();
            m_pos += 1;
            return result;
        }

        /**
         * \brief parse the map properties
        */
        MapProperties parseMapProperties() {
            std::map<std::string, std::unique_ptr<Entry>> entries;
            ignoreLines();
            nextExpect(Token::LBRACKET);
            nextExpect(Token::IDENTIFIER);
            nextExpect(Token::RBRACKET);
            nextExpect(Token::EOL);
            ignoreLines();
            while(!end() && current().kind == Token::MINUS) {
                nextExpect(Token::MINUS);
                const auto name = nextExpect(Token::IDENTIFIER).text;
                nextExpect(Token::EQUAL);
                entries.insert(std::make_pair(name, parseEntry()));
            }
            auto result = MapProperties {
                .mapName = entries.at("name")->as_string(),
                .texturePath = entries.at("texture")->as_string(),
                .heightMapPath = entries.at("heightmap")->as_string(),
                .enemyPositions = entries.at("enemies")->as_positions()
            };
            return result;
        }

        /**
         * \brief parse an entry in the configuration file
        */
        std::unique_ptr<Entry> parseEntry() {
            while(!end() && current().kind != Token::EOL) {
                if(current().kind == Token::STRING) {
                    const auto str = current().text;
                    auto result = std::make_unique<StringEntry>(StringEntry(current().text));
                    nextExpect(Token::STRING);
                    return result;
                } else if(current().kind == Token::LBRACKET) {
                    auto result = parsePositions();
                    nextExpect(Token::RBRACKET);
                    return std::make_unique<PositionsEntry>(PositionsEntry(result));
                }
            }
        }

        /**
         * \brief parse an array object representing an array of positions
        */
        std::vector<std::pair<float, float>> parsePositions() {
            std::vector<std::pair<float, float>> result;
            nextExpect(Token::LBRACKET);
            while(!end() && current().kind != Token::RBRACKET) {
                ignoreLines();
                if(current().kind == Token::LPAREN) {
                    nextExpect(Token::LPAREN);
                    ignoreLines();
                    const auto left = nextExpect(Token::NUMBER).number;
                    ignoreLines();
                    nextExpect(Token::COMA);
                    ignoreLines();
                    const auto right = nextExpect(Token::NUMBER).number;
                    ignoreLines();
                    nextExpect(Token::RPAREN);
                    ignoreLines();
                    if(current().kind != Token::RBRACKET) {
                        nextExpect(Token::COMA);
                    }
                    result.push_back(std::make_pair(left, right));
                }
                ignoreLines();
            }
            return result;
        }
    };
}

Configuration parseConfig(const std::string& path) {
    std::ifstream file(path);
    auto tokens = tokenize(file);
    auto parser = ConfigurationParser(tokens);
    return parser.parse();
}

}