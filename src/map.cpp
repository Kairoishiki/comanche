#include <SDL2/SDL.h>
#include <iostream>
#include <vector>
#include "../lib/glm/glm.hpp"
#include "../lib/glm/geometric.hpp"
#include "../lib/glm/trigonometric.hpp"
#include "../lib/glm/gtx/rotate_vector.hpp"
#include "sdlwrapper/surface.hpp"
#include "map.hpp"
#include "entities/enemy.hpp"

Map::Map(const std::string& diffuse, const std::string& heightmap) {
    SDL::Surface df(diffuse);
    SDL::Surface hm(heightmap);

    if (df->w != hm->w || df->h != hm->h) {
	    SDL::panic("Diffuse map and height map have different dimensions.");
    }

    if ((hm->w & (hm->w - 1)) != 0) {
	    SDL::panic("Images width must be power of two.");
    }

    m_width = hm->w;
    m_height = hm->h;
    m_color = std::vector<SDL::Color::Color>();
    m_altitude = std::vector<int>();

    for(size_t x = 0; x < hm->w ; x += 1) {
        for(size_t y = 0 ; y < hm->h ; y += 1) {
            const auto textureColor = df.getPixel(x, y);
            const auto heightColor = hm.getPixel(x, y).r;
            m_color.push_back(textureColor);
            m_altitude.push_back(heightColor);
        }
    }
    m_ready = true;
    m_colorMap = std::move(df);
    m_heightMap = std::move(hm);
}

void Map::render(const SDL::Window& window, SDL::Renderer& ctx, const Camera& camera, Enemy& enemy) {
    glm::vec2 fovleft = glm::rotate(camera.view(), camera.fov() / 2.f);
    glm::vec2 fovright = glm::rotate(camera.view(), - camera.fov() / 2.f);
    // vector representig the horizon direction
    glm::vec2 horizonVector = glm::vec2 {
        glm::cos(camera.angleZ()),
        glm::sin(camera.angleZ())
    };
    // the number of columns to display is maximum when equal to the length of the screen diagonal
    const int nColonnes = glm::distance(glm::vec2(0.f, 0.f), glm::vec2(window.width(), window.height()));
    // the point representing the middle of the screen
    auto screenMiddle = glm::vec2(window.width() / 2.f, window.height() / 2.f);
    // the point of horizon on screen. Try changing 'window.height() / 4.f' to 'window.height() / 2.f'
    auto horizonMiddle = glm::vec2(window.width() / 2.f, window.height() / 4.f);
    // the point where horizonMiddle should be relative to screenMiddle and current tilt angle
    auto diff = glm::rotate(horizonMiddle - screenMiddle, camera.angleZ());
    // the point where the horizon begins (can be outside the screen)
    auto horizonBegin = -horizonVector * float(nColonnes) / 2.f + screenMiddle + diff;
    // y-buffer, filled with the height of the window
    static std::vector<float> heights(nColonnes);
    std::fill(heights.begin(), heights.end(), window.height());
    float optiz = 0.5f;
    for(float deltaD = 1 ; deltaD < camera.d() ; deltaD += optiz) {
        // computes left and right point on the fov
        auto leftPoint = fovleft * deltaD;
        auto rightPoint = fovright * deltaD;
        // render the columns
        for(int i = 0 ; i < nColonnes ; i += 1) {
            const auto f = 1.f / nColonnes;
            const auto point = (leftPoint * float(i) * f + rightPoint * (1.f - float(i) * f));
            // gets the height of the point to render, color is retrieved if and only if the point is rendered
            const auto onMapx = point.x + camera.position().x;
            const auto onMapy = point.y - camera.position().y;
            const auto height = getHeightAt(onMapx, onMapy);
            // auto final_y = (float)(camera.Position.z / 2 - height) / deltaD * 500 + horizon;
            const auto renderedY = (float)(camera.position().z - float(height)) / deltaD * 500.f;
            // we draw from the front : we only draw if the point to be renderer is higher than the last one
            if(renderedY < heights[i]) {
                // create a blur effect when objects are far from the camera
                const auto ciel = SDL::Color::BLUESKY;
                // auto color = getColorAt(onMapx, onMapy);
                auto c1 = getColorAt(onMapx, onMapy);
                auto c2 = getColorAt(onMapx + 1, onMapy);
                auto c3 = getColorAt(onMapx - 1, onMapy);
                auto c4 = getColorAt(onMapx, onMapy + 1);
                auto c5 = getColorAt(onMapx, onMapy - 1);
                SDL::Color::Color color{
                    (1 * (c1.r) + 1 * (c2.r + c3.r + c4.r + c5.r)) / 5.f,
                    (1 * (c1.g) + 1 * (c2.g + c3.g + c4.g + c5.g)) / 5.f,
                    (1 * (c1.b) + 1 * (c2.b + c3.b + c4.b + c5.b)) / 5.f,
                    255
                };
                color.r = color.r * (1 - deltaD / camera.d()) + ciel.r * (deltaD / camera.d());
                color.g = color.g * (1 - deltaD / camera.d()) + ciel.g * (deltaD / camera.d());
                color.b = color.b * (1 - deltaD / camera.d()) + ciel.b * (deltaD / camera.d());
                ctx.setColor(color);
                // drawVector is the perpendicular vector regarding to horizonVector
                const auto drawVector = glm::rotate(horizonVector, glm::half_pi<float>());
                // draw points up and down are the point between which we must draw
                const auto horizonPoint = horizonVector * float(i) + horizonBegin;
                const auto drawPointUp = horizonPoint + drawVector * renderedY;
                // const auto drawPointDown = drawPointUp + drawVector * (heights[i] - renderedY + 1);
                const auto drawPointDown = drawPointUp + drawVector * (heights[i] - renderedY + 1);
                ctx.drawLine(drawPointUp.x + 1, drawPointUp.y, drawPointDown.x + 1, drawPointDown.y); // this fixes rendering holes
                ctx.drawLine(drawPointUp.x, drawPointUp.y, drawPointDown.x, drawPointDown.y);

		// render enemy
		//for(auto enemy : enemies) {
		//  if (enemy.inArray(onMapx, onMapy, m_width, m_height)) {
		//      enemy.setVisible(true);
		//      enemy.setCamPosition(drawVector, horizonPoint, renderedY);
		//  }
		//}
		if (enemy.inArray(onMapx, onMapy, m_width, m_height)) {
		    enemy.setVisible(true);
		    enemy.setCamPosition(drawVector, horizonPoint, renderedY);
		}
                // end render enemy

                heights[i] = renderedY;
            }
        }
        optiz = std::min(optiz + deltaD / camera.d(), 7.f);
        
    }
}

int Map::width() const {
    return m_width;
}

int Map::height() const {
    return m_height;
}

std::pair<SDL::Color::Color, int> Map::getAt(int x, int y) const {
    x %= m_width;
    y %= m_height;
    x = (x + m_width) % m_width;
    y = (y + m_height) % m_height;
    const auto color = m_color[x * m_width + y];
    const auto height = m_altitude[x * m_width + y];
    return {color, height};
}

int Map::getHeightAt(int x, int y) const {
    x %= m_width;
    y %= m_height;
    x = (x + m_width) % m_width;
    y = (y + m_height) % m_height;
    const auto height = m_altitude[x * m_width + y];
    return height;
}

int Map::size() const {
    return m_altitude.size();
}

SDL::Color::Color Map::getColorAt(int x, int y) const {
    x %= m_width;
    y %= m_height;
    x = (x + m_width) % m_width;
    y = (y + m_height) % m_height;
    const auto color = m_color[x * m_width + y];
    return color;
}