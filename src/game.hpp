#pragma once
#include <SDL2/SDL.h>
#include <string>
#include <functional>
#include "map.hpp"
#include "sdlwrapper/sdlwrapper.hpp"
#include "entities/helicopter.hpp"
#include "menu.hpp"
#include "configuration.hpp"

enum class Frame {
    menu_frame,
    game_frame
};

class Game {
public:
    Game(const std::string& cMap, const std::string& hMap, config::Configuration& config);

    /**
     * \brief loads the game given the properties of the map chosen
     * \param map the informations relative to the map chosen
    */
    void loadGame(const config::MapProperties& map);

    /**
     * \brief runs the game : main game loop
    */
    void run();

    /**
     * \brief process all the SDL events
    */
    void ProcessSDLEvents();

    /**
     * \brief update the informations of the game entities
    */
    void UpdateInput();

    /**
     * \brief process the key down event
     * \param ev the SDL keyboard event
    */
    void ProcessKeyDown(SDL_KeyboardEvent* ev);

    /**
     * \brief process the key up event
     * \param ev the SDL keyboard event
    */
    void ProcessKeyUp(SDL_KeyboardEvent* ev);

    /**
     * \brief getter for the map that is played
    */
    Map& map();

    /**
     * \brief getter for the window of the game
    */
    SDL::Window& window();

    /**
     * \brief getter for the renderer used by the game
    */
    SDL::Renderer& renderer();

    /**
     * \brief getter for the font manager used by the game
    */
    TTF::FontManager& fontManager();
private:
    Map m_map;
    bool m_running;
    Camera m_cam;
    Helicopter m_helicopter;
    Enemy m_enemy;
    //std::vector<Enemy> m_enemies;
    //int m_deltaTime;
    SDL::Window m_window;
    SDL::Renderer m_renderer;
    TTF::FontManager m_fontManager;
    Menu m_menu;
    std::reference_wrapper<config::Configuration> m_configuration;
    Frame m_frame;
};
