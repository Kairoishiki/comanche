#pragma once
#include <vector>
#include <string>
#include "sdlwrapper/sdlwrapper.hpp"
#include "camera.hpp"
#include "entities/enemy.hpp"

class Enemy;

class Map {
public:
    Map() = default;
    Map(const std::string&, const std::string&);

    /**
     * \brief renders the map on screen
     * \param window the window
     * \param renderer the renderer
     * \param camera the camera looking at the scene
     * \param enemies the enemies to render
    */
    void render(const SDL::Window& window, SDL::Renderer& renderer, const Camera& camera, Enemy& enemies);

    /**
     * \brief getter for the map width
     * \return the width of the map
    */
    int width() const;

    /**
     * \brief getter for the map height
     * \return the height of the map
    */
    int height() const;

    /**
     * \brief the size in terms of numbe of pixels
     * \return the size of the map in pixels
    */
    int size() const;

    /**
     * \brief get the color and the height at any given point of the map
     * \param x the x coordinate
     * \param y the y coordinate
     * \return a pair of (color, height)
    */
    std::pair<SDL::Color::Color, int> getAt(int x, int y) const;

    /**
     * \brief get the height at any given point on map
     * \param x the x coordinate
     * \param y the y coordinate
     * \return the height on the map
    */
    int getHeightAt(int x, int y) const;

    /**
     * \brief get the color at any given point on map
     * \param x the x coordinate
     * \param y the y coordinate
     * \return the color of the terrain
    */
    SDL::Color::Color getColorAt(int x, int y) const;
private:
    SDL::Surface m_colorMap;
    SDL::Surface m_heightMap;
    std::vector<SDL::Color::Color> m_color;
    std::vector<int> m_altitude;
    SDL::Window* m_window;
    bool m_ready;
    int m_width, m_height;
};
