#include "menu.hpp"
#include "game.hpp"

Menu::Menu(Game* game, const config::Configuration* config):
    m_game(game),
    m_configuration(config),
    m_selected(0)
{
    for(const auto& map : config->availableMaps) {
        m_mapImages.push_back(SDL::Surface(map.texturePath).toTexture(m_game->renderer()));
    }
}

void Menu::processKeyDown(SDL_KeyboardEvent* event) {
    SDL_Scancode code = event->keysym.scancode;
    switch(code) {
        case SDL_SCANCODE_UP:
        case SDL_SCANCODE_DOWN:
        case SDL_SCANCODE_LEFT:
        case SDL_SCANCODE_RIGHT:
        case SDL_SCANCODE_TAB:
            handleChangeSelection(code);
            break;
        case SDL_SCANCODE_RETURN:
            m_game->loadGame(m_configuration->availableMaps[m_selected]);
    }
}

void Menu::handleChangeSelection(SDL_Scancode code) {
    const auto mapCount = m_configuration->availableMaps.size();
    const auto add = (code == SDL_SCANCODE_UP || code == SDL_SCANCODE_LEFT || code == SDL_SCANCODE_TAB) ? -1 : 1;
    m_selected = (m_selected + add) % mapCount;
    m_selected = (m_selected + mapCount) % mapCount;
}

void Menu::render() {
    TTF::Font* font = &m_game->fontManager()[{"arial", 50}];
    auto& renderer = m_game->renderer();

    const auto windowW = m_game->window().width();
    const auto windowH = m_game->window().height();

    constexpr auto selectLeftOffset = 20;
    constexpr auto selectEntryWidth = 300;
    constexpr auto selectEntryHeight = 60;
    constexpr auto selectedSignSize = 10;
    constexpr auto mapSize = 300;
    constexpr auto separationBeginLeft = selectLeftOffset * 3 + selectEntryWidth;
    auto mapBeginLeft = separationBeginLeft + (windowW - separationBeginLeft) / 2 - mapSize / 2;
    constexpr auto titleBegin = 50;
    constexpr auto selectionSectionBegin = 200;
    int begin = selectionSectionBegin;
    // render title
    renderer.setColor({220, 220, 220, 255});
    auto& titleFont = m_game->fontManager()[{"arial", 50}];
    auto [titleW, titleH] = titleFont.sizeText("COMANCHE");
    renderer.renderText("COMANCHE", titleFont, windowW / 2 - titleW / 2, titleBegin);
    renderer.drawLine(selectLeftOffset, titleBegin * 2 + titleH, windowW - selectLeftOffset, titleBegin * 2 + titleH);
    // render selection list title
    const auto selectionTitle = "Map Selection (press enter)";
    renderer.setColor({200, 200, 200, 255});
    auto& selectionTitleFont = m_game->fontManager()[{"arial", 20}];
    auto [selectionTitleW, selectionTitleH] = selectionTitleFont.sizeText(selectionTitle);
    renderer.renderText(selectionTitle, selectionTitleFont, selectLeftOffset, begin);
    // render separation
    renderer.setColor({150, 150, 150, 255});
    renderer.drawLine(separationBeginLeft, begin, separationBeginLeft, windowH - 20);
    // render selection choices
    begin += selectionTitleH + 20;
    int i = 0;
    for(const auto& map : m_configuration->availableMaps) {
        if(i == m_selected) {
            font = &m_game->fontManager()[{"arial", 30}];
            renderer.setColor({30, 30, 30, 255});
            renderer.drawRectangle(selectLeftOffset, begin, selectEntryWidth, selectEntryHeight);
            renderer.setColor({255, 255, 255, 255});
        } else {
            font = &m_game->fontManager()[{"arial", 30}];
            renderer.setColor({20, 20, 20, 255});
            renderer.drawRectangle(selectLeftOffset, begin, selectEntryWidth, selectEntryHeight);
            renderer.setColor({150, 150, 150, 255});
        }
        const auto [w, h] = font->sizeText(map.mapName);
        const auto x = selectLeftOffset + (selectEntryWidth / 2) - w / 2;
        const auto y = begin + (selectEntryHeight / 2) - h / 2;
        renderer.renderText(map.mapName, *font, x, y);
        if(i == m_selected) {
            renderer.drawRectangle(selectLeftOffset + 10, begin + (selectEntryHeight / 2) - selectedSignSize / 2, selectedSignSize, selectedSignSize);
        }
        
        begin += selectEntryHeight;
        i += 1;
    }
    // display map
    auto& selected = m_mapImages[m_selected];
    selected.display(selected.getRect(), {mapBeginLeft, selectionSectionBegin, mapSize, mapSize});
}