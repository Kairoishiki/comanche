#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include "sdlinit.hpp"
#include "utils.hpp"

void SDL::initSDL() {
    if(SDL_Init(SDL_INIT_VIDEO) < 0) {
        SDL::panic("Unable to init SDL");
    }
}

void SDL::initTTF() {
    if(TTF_Init() == -1) {
        SDL::panic("Unable to init SDL_TTF");
    }
}

void SDL::initAudio() {
    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) == -1) {
        SDL::panic(Mix_GetError());
    }
}

void SDL::init() {
    SDL::initSDL();
    SDL::initTTF();
    SDL::initAudio();
}