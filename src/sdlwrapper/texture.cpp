#include "texture.hpp"
#include "renderer.hpp"
#include "utils.hpp"
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_image.h>
#include <iostream>

SDL::Texture::Texture(const std::string &filename, Renderer& renderer) {
    SDL_Surface* surface = IMG_Load(filename.c_str());
    if(surface == nullptr) {
        SDL::panic("Unable to load texture");
    }
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer.c_ptr(), surface);
    SDL_FreeSurface(surface);
    SDL_QueryTexture(texture, nullptr, nullptr, &m_width, &m_height);
    m_renderer = renderer.c_ptr();
    m_texture = ptr_t(texture, SDL_DestroyTexture);
}

SDL::Texture::Texture(SDL_Texture* texture, Renderer& renderer):
    m_texture(texture, SDL_DestroyTexture),
    m_renderer(renderer.c_ptr())
{
    SDL_QueryTexture(texture, nullptr, nullptr, &m_width, &m_height);
}

SDL::Texture SDL::Texture::from(SDL_Texture* texture, Renderer& renderer) {
    return SDL::Texture(texture, renderer);
}

int SDL::Texture::width() const {
    return m_width;
}

int SDL::Texture::height() const {
    return m_height;
}

SDL_Texture* SDL::Texture::c_ptr() {
    return m_texture.get();
}

void SDL::Texture::display(int x, int y) {
    SDL::Rect dst {x, y, m_width, m_height};
    SDL_RenderCopy(m_renderer, m_texture.get(), NULL, &dst);
}

void SDL::Texture::display(SDL::Rect src, SDL::Rect dest) {
    SDL_RenderCopy(m_renderer, m_texture.get(), &src, &dest);
}

SDL::Rect SDL::Texture::getRect() const {
    return SDL::Rect{ 0, 0, m_width, m_height };
}
