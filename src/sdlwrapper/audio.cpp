#include "audio.hpp"
#include "utils.hpp"

AUDIO::Music::Music(Mix_Music* music):
    m_music(music, Mix_FreeMusic)
{

}

AUDIO::Music::Music(const std::string& filename) {
    Mix_Music* music = Mix_LoadMUS(filename.c_str());
    if(music == nullptr) {
        SDL::panic("Unable to load music");
    }
    m_music = ptr_t(music, Mix_FreeMusic);
}

Mix_Music* AUDIO::Music::c_ptr() {
    return m_music.get();
}