#pragma once

#include <SDL2/SDL_ttf.h>
#include <memory>
#include <string_view>
#include <string>
#include <functional>
#include <map>
#include <optional>

namespace TTF {

class Font {
public:
    Font() = default;
    Font(const std::string& filename, int fontSize);
    Font(Font&&) = default;

    /**
     * \brief return the size needed to display the given text on screen
     * \param text the text
     * \return a pair (width, height) needed to render the text on screen
    */
    std::pair<int, int> sizeText(const std::string& text) const;

    /**
     * \brief getter for the inner pointer to the TTF_Font
     * \return the font
    */
    TTF_Font* c_ptr();
private:
    using ptr_t = std::unique_ptr<TTF_Font, std::function<void(TTF_Font*)>>;
    ptr_t m_font;
    std::string m_filename;
    int m_fontSize;
};

class FontManager {
private:
    using key_t = std::pair<std::string, int>;
    using map_t = std::map<key_t, Font>;
public:
    map_t m_data;
    FontManager() = default;
    void openFont(const std::string& filename, const std::string& name, int fontSize);
    Font& operator[](const key_t& key);
};

}