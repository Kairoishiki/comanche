#pragma once

#include <SDL2/SDL.h>
#include <ostream>

namespace SDL {
    namespace Color {
        using Color = SDL_Color;
        extern const Color RED;
        extern const Color GREEN;
        extern const Color BLUE;
        extern const Color BLUESKY;
        extern const Color BLACK;
        extern const Color WHITE;
    }
}

std::ostream& operator<<(std::ostream& out, const SDL::Color::Color& color);
