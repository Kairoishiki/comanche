#pragma once

#include <SDL2/SDL.h>
#include <string>
#include <string_view>
#include <stdexcept>

namespace SDL {

using Rect = SDL_Rect;

class sdl_exception : public std::runtime_error {
private:
    char const* m_msg;
public:
    sdl_exception(char const* message);
    virtual const char* what();
};

/**
 * \brief quit SDL. Wrapper around SDL_Quit
*/
void quit();

/**
 * \brief panic and force quit the program
 * \param errmsg the error message to be displayed
*/
void panic(std::string_view errmsg);

/**
 * \brief display error on std::cerr stream
 * \param errmsg the error message to be displayed
*/
void logError(std::string_view errmsg);

/**
 * \brief controls the FPS of the game. This function must be called at each frame.
 * \param FPS the maximal number of FPS authorized
*/
void waitFPS(Uint32 FPS);

}