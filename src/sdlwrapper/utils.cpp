#include "utils.hpp"
#include <SDL2/SDL_error.h>
#include <SDL2/SDL_log.h>
#include <cstdlib>
#include <iostream>
#include <SDL2/SDL.h>
#include <string>
#include <string_view>


SDL::sdl_exception::sdl_exception(const char* msg): std::runtime_error(msg) {}

const char* SDL::sdl_exception::what() {
    return m_msg;
}


void SDL::logError(std::string_view errmsg) {
    std::cerr << errmsg <<  ": " <<  SDL_GetError() << std::endl;
}

void SDL::quit() {
    SDL_Quit();
}

void SDL::panic(std::string_view errmsg) {
    SDL::logError(errmsg);
    SDL::quit();
    exit(EXIT_FAILURE);
}

void SDL::waitFPS(Uint32 FPS) {
    const auto waitTime = 1000.f / FPS;
    static Uint32 lastTick = 0;
    Uint32 currentTick = SDL_GetTicks();
    Uint32 diff = currentTick - lastTick;
    if(diff < waitTime) {
        SDL_Delay(waitTime - diff);
    } else {
        SDL_Delay(waitTime);
    }
    lastTick = lastTick;
}