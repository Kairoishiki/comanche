#include "window.hpp"
#include "sdlwrapper.hpp"
#include "utils.hpp"
#include <SDL2/SDL_video.h>

SDL::Window::Window(const std::string& name, int x, int y, int w, int h, Uint32 flags) {
    SDL_Window* window = SDL_CreateWindow(name.c_str(), x, y, w, h, flags);
    if(window == nullptr) {
        SDL::panic("Unable to create window");
    }
    m_window = ptr_t(window, SDL_DestroyWindow);
    m_width = w;
    m_height = h;
}

SDL_Window* SDL::Window::c_ptr() {
    return m_window.get();
}

int SDL::Window::width() const {
    return m_width;
}

int SDL::Window::height() const {
    return m_height;
}