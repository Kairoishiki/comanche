#pragma once

#include <SDL2/SDL.h>
#include <string>
#include <memory>
#include <functional>

namespace SDL {

class Window {
public:
    Window(const std::string& name, int x, int y, int w, int h, Uint32 flags);

    /**
     * \brief get the inner C pointer to the SDL_Window
     * \return the C pointer
    */
    SDL_Window* c_ptr();

    /**
     * \brief getter for the width of the window
     * \return the width of the window
    */
    int width() const;

    /**
     * \brief getter for the height of the window
     * \return the height of the window
    */
    int height() const;
private:
    using ptr_t = std::unique_ptr<SDL_Window, std::function<void(SDL_Window*)>>;
    int m_width;
    int m_height;
    ptr_t m_window;
};

}