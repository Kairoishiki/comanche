#pragma once

#include <SDL2/SDL_mixer.h>
#include <string>
#include <memory>
#include <functional>

namespace AUDIO
{

class Music {
public:
    Music(Music&&) = default;
    Music(const Music&) = delete;
    Music(Mix_Music* musique);
    Music(const std::string& filename);
    Mix_Music* c_ptr();
private:
    using ptr_t = std::unique_ptr<Mix_Music, std::function<void(Mix_Music*)>>;
    ptr_t m_music;
};

}
