#pragma once

#include <memory>
#include <string>
#include <functional>
#include <SDL2/SDL.h>
#include "renderer.hpp"
#include "texture.hpp"
#include "color.hpp"

namespace SDL
{

class Surface {
public:
    Surface() = default;
    Surface(Surface&&) = default;
    Surface& operator=(Surface&&) = default;
    Surface(const Surface&) = delete;
    Surface(const std::string& filename);

    /**
     * \brief Wraps a raw SDL_Surface into a Surface object
     * \param surface the raw surface to wrap
     * \return a Surface object wrapping the raw surface
    */
    static Surface from(SDL_Surface* surface);

    /**
     * \brief converts a SDL_Surface into a SDL_Texture
     * \param renderer the renderer
     * \return the texture
    */
    SDL::Texture toTexture(Renderer& renderer) const;

    /**
     * \brief getter for the inner C pointer to the raw SDL_Surface
     * \return a raw pointer to the SDL_Surface
    */
    SDL_Surface* c_ptr();

    /**
     * \brief get the color of any pixel in the surface
     * \param x the x position
     * \param y the y position
     * \return the color of the pixel at (x, y)
    */
    SDL::Color::Color getPixel(size_t x, size_t y);

    /**
     * \brief forward operations on SDL_Surface
    */
    SDL_Surface* operator->();
private:
    Surface(SDL_Surface* surface);
    using ptr_t = std::unique_ptr<SDL_Surface, std::function<void(SDL_Surface*)>>;
    ptr_t m_surface;
};

}
