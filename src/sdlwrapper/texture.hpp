#pragma once

#include <SDL2/SDL.h>
#include <memory>
#include <functional>
#include <string>
#include "utils.hpp"

namespace SDL {
class Renderer;

class Texture {
public:
    Texture(const std::string& filename, Renderer& renderer);
    Texture(Texture&&) = default;
    Texture(const Texture&) = delete;

    /**
     * \brief wraps a SDL_Texture into an Texture object
     * \param texture the raw texture to wrap
     * \param renderer the renderer associated to the texture
     * \return a new Texture object wrapping a row SDL_Texture
    */
    static Texture from(SDL_Texture* texture, Renderer& renderer);

    /**
     * \brief getter for the inner pointer to the SDL_Texture
     * \return the pointer to the texture
    */
    SDL_Texture* c_ptr();

    /**
     * \brief getter for the width of the texture
     * \return the width of the texture
    */
    int width() const;

    /**
     * \brief getter for the height of the texture
     * \return the height of the texture
    */
    int height() const;

    /**
     * \brief displays the whole texture on screen at a given position
     * \param x the x position on screen
     * \param y the y position on screen
     * The position must be given in the SDL coordinate system.
    */
    void display(int x, int y);

    /**
     * \brief displays a part of the texture into a specified area on screen
     * \param src the part of the texture to copy on screen
     * \param dest the area on screen where to copy src
    */
    void display(SDL::Rect src, SDL::Rect dest);

    /**
     * \brief gets the infos about the texture position and size in a SDL_Rect
    */
    SDL::Rect getRect() const;
private:
    Texture(SDL_Texture* texture, Renderer& renderer);
    using ptr_t = std::unique_ptr<SDL_Texture, std::function<void(SDL_Texture*)>>;
    ptr_t m_texture;
    SDL_Renderer* m_renderer;
    int m_width;
    int m_height;
};

}