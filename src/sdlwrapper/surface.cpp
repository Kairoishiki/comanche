#include "surface.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>

SDL::Surface::Surface(SDL_Surface* surface):
    m_surface(surface, SDL_FreeSurface)
{

}

SDL_Surface* SDL::Surface::c_ptr() {
    return m_surface.get();
}

SDL::Surface::Surface(const std::string& filename) {
    SDL_Surface* surface = IMG_Load(filename.c_str());
    if(surface == nullptr) {
        SDL::panic("Unable to load texture");
    }
    m_surface = ptr_t(surface, SDL_FreeSurface);
}

SDL::Surface SDL::Surface::from(SDL_Surface* surface) {
    return SDL::Surface(surface);
}

SDL::Texture SDL::Surface::toTexture(Renderer& renderer) const {
    return SDL::Texture::from(SDL_CreateTextureFromSurface(renderer.c_ptr(), m_surface.get()), renderer);
}

SDL_Surface* SDL::Surface::operator->() {
    return c_ptr();
}

SDL::Color::Color SDL::Surface::getPixel(size_t x, size_t y) {
    auto surface = c_ptr();
    // image infos
    auto width = surface->w;
    auto height = surface->h;
    if(x >= width || y >= height) {
        throw "Invalid pixel position";
    }
    auto pitch = surface->pitch; // length of a surface scanline in bytes
    auto palette = surface->format->palette;
    int bpp = surface->format->BytesPerPixel;
    int bitspp = surface->format->BitsPerPixel;
    auto imageData = surface->pixels;
    // pointer to data
    uint8_t * dataPtr = static_cast<uint8_t*>(imageData) + y * pitch + x * bpp;
    
    switch(bitspp) {
        case 8: { // each pixel in data is a reference to the color palette
            uint8_t colorPtr = *dataPtr;
            return palette->colors[colorPtr];
        }
        case 16: {
            uint8_t ar = dataPtr[0];
            uint8_t gb = dataPtr[1];
            uint8_t r = ar & 0b1111;
            uint8_t g = (gb & 0b00001111) >> 4;
            uint8_t b = gb & 0b1111;
            uint8_t a = (ar & 0b00001111);
            return {a, r, g, b};
        }
        case 24: {
            uint8_t r = dataPtr[2];
            uint8_t g = dataPtr[1];
            uint8_t b = dataPtr[0];
            return {r, g, b, 255};
        }
        case 32:{
            uint8_t r = dataPtr[2];
            uint8_t g = dataPtr[1];
            uint8_t b = dataPtr[0];
            uint8_t a = dataPtr[3];
            return {r, g, b, a};
        }
    }
}
