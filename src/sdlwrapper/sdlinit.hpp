#pragma once

namespace SDL {

/**
 * \brief initialise SDL and SDL submodules used for the game
*/
void init();

/**
 * \brief initialise SDL
*/
void initSDL();

/**
 * \brief initialise TTF submodule
*/
void initTTF();

/**
 * \brief initialise Audio module
*/
void initAudio();

}