#pragma once

#include <SDL2/SDL.h>
#include <memory>
#include <functional>
#include "utils.hpp"
#include "ttf.hpp"
#include "color.hpp"

namespace SDL {
class Window;

/**
 * \brief C++ wrapper around SDL_Renderer
*/
class Renderer {
public:
    /**
     * \brief Constructor, wrapper around SDL_CreateRenderer
     * \param window the window where rendering is displayed
     * \param index the index of the rendering driver to initialize, or -1 to initialize the first one supporting the requested flags
     * \param flags 0, or one or more SDL_RendererFlags OR'd together
    */
    Renderer(Window& window, int index, Uint32 flags);

    /**
     * \brief getter of the inner C raw pointer on the SDL_Renderer
     * \return the raw pointer on the SDL_Renderer
    */
    SDL_Renderer* c_ptr();

    /**
     * \brief clears the current rendering context with the draw color. Wrapper around SDL_RendererClear
    */
    void clear();

    /**
     * \brief update the screen. Wrapper around SDL_RenderPresent
    */
    void renderPresent();

    /**
     * \brief sets the rendering color. Wrapper around SDL_SetRenderDrawColor
     * \param color color to be set
    */
    void setColor(Color::Color color);

    /**
     * \brief gets the current rendering color
     * \return the rendering color
    */
    Color::Color getColor() const;

    /**
     * \brief renders text
     * \param text text to be rendered
     * \param font font of the text
     * \param x x-coordinate of the text position on screen (in SDL coordinates system)
     * \param y y-coordinate of the text position on screen (in SDL coordinates system)
    */
    void renderText(const std::string& text, TTF::Font& font, int x, int y);
    /**
     * \brief renders text
     * \param text text to be rendered
     * \param font font of the text
     * \param x x-coordinate of the text position on screen (in SDL coordinates system)
     * \param y y-coordinate of the text position on screen (in SDL coordinates system)
     * \param color color of the text
    */
    void renderText(const std::string& text, TTF::Font& font, int x, int y, Color::Color color);

    /**
     * \brief renders a line. Wrapper around SDL_RenderDrawLine
     * \param x1 the x coordinate of the start point
     * \param y1 the y coordinate of the start point
     * \param x2 the x coordinate of the end point
     * \param y2 the y coordinate of the end point
    */
    void drawLine(int x1, int y1, int x2, int y2);

    /**
     * \brief renders a rectangle. Wrapper around SDL_RenderDrawRect
     * \param x1 the x coordinate of the start point
     * \param y1 the y coordinate of the start point
     * \param w the width of the rectangle
     * \param h the height of the rectangle
    */
    void drawRectangle(int x, int y, int w, int h);

    /**
     * \brief renders a circle on screen
     * \param centerx the center point x coordinate
     * \param centery the center point y coordinate
     * \param radius the circle radius
    */
    void drawCircle(int centerx, int centery, int radius);

    static const Color::Color DEFAULT_COLOR; /** default draw color */
private:
    using ptr_t = std::unique_ptr<SDL_Renderer, std::function<void(SDL_Renderer*)>>; /** unique_ptr type for SDL_Renderer */
    ptr_t m_renderer; /** smart pointer to the C SDL_Renderer */
    Color::Color m_color; /** current draw color of the renderer */
};

}