#include "renderer.hpp"
#include "window.hpp"
#include "texture.hpp"
#include "utils.hpp"
#include "surface.hpp"
#include <SDL2/SDL_render.h>

const SDL::Color::Color SDL::Renderer::DEFAULT_COLOR = SDL::Color::BLACK;

SDL::Renderer::Renderer(Window& window, int index, Uint32 flags) {
    SDL_Renderer* renderer = SDL_CreateRenderer(window.c_ptr(), index, flags);
    if(renderer == nullptr) {
        SDL::panic("Unable to create renderer");
    }
    m_renderer = ptr_t(renderer, SDL_DestroyRenderer);
    m_color = DEFAULT_COLOR;
}

SDL_Renderer* SDL::Renderer::c_ptr() {
    return m_renderer.get();
}

void SDL::Renderer::clear() {
    SDL_RenderClear(m_renderer.get());
}

void SDL::Renderer::renderPresent() {
    SDL_RenderPresent(m_renderer.get());
}

void SDL::Renderer::renderText(const std::string& text, TTF::Font& font, int x, int y) {
    auto [w, h] = font.sizeText(text);
    SDL::Rect rect{x, y, w, h};
    auto texture = SDL::Surface::from(TTF_RenderUTF8_Blended(font.c_ptr(), text.c_str(), m_color)).toTexture(*this);
    SDL_RenderCopy(m_renderer.get(), texture.c_ptr(), NULL, &rect);
}

void SDL::Renderer::renderText(const std::string& text, TTF::Font& font, int x, int y, SDL::Color::Color color) {
    auto [w, h] = font.sizeText(text);
    SDL::Rect rect{x, y, w, h};
    auto texture = SDL::Surface::from(TTF_RenderUTF8_Blended(font.c_ptr(), text.c_str(), color)).toTexture(*this);
    SDL_RenderCopy(m_renderer.get(), texture.c_ptr(), NULL, &rect);
}

void SDL::Renderer::drawLine(int x1, int y1, int x2, int y2) {
    SDL_RenderDrawLine(m_renderer.get(), x1, y1, x2, y2);
}

void SDL::Renderer::setColor(Color::Color color) {
    SDL_SetRenderDrawColor(m_renderer.get(), color.r, color.g, color.b, color.a);
    m_color = color;
}

void SDL::Renderer::drawRectangle(int x, int y, int w, int h) {
    SDL_Rect rect = {x, y, w, h};
    SDL_RenderFillRect(m_renderer.get(), &rect);
}

SDL::Color::Color SDL::Renderer::getColor() const {
    return m_color;
}

void SDL::Renderer::drawCircle(int centreX, int centreY, int radius) {
    // this algorithm has been found online
    const auto diameter = (radius * 2);
    const auto renderer = m_renderer.get(); 
    int x = (radius - 1);
    int y = 0;
    int tx = 1;
    int ty = 1;
    int error = (tx - diameter);    
    while (x >= y) {
        SDL_RenderDrawPoint(renderer, centreX + x, centreY - y);
        SDL_RenderDrawPoint(renderer, centreX + x, centreY + y);
        SDL_RenderDrawPoint(renderer, centreX - x, centreY - y);
        SDL_RenderDrawPoint(renderer, centreX - x, centreY + y);
        SDL_RenderDrawPoint(renderer, centreX + y, centreY - x);
        SDL_RenderDrawPoint(renderer, centreX + y, centreY + x);
        SDL_RenderDrawPoint(renderer, centreX - y, centreY - x);
        SDL_RenderDrawPoint(renderer, centreX - y, centreY + x); 
        if (error <= 0) {
            ++y;
            error += ty;
            ty += 2;
        }    
        if (error > 0) {
            --x;
            tx += 2;
            error += (tx - diameter);
        }
    }
}