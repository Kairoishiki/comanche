#include "color.hpp"

using namespace SDL;

const Color::Color Color::BLACK{0, 0, 0, 255};
const Color::Color Color::WHITE{255, 255, 255, 255};
const Color::Color Color::RED{255, 0, 0, 255};
const Color::Color Color::GREEN{0, 255, 0, 255};
const Color::Color Color::BLUE{0, 0, 255, 255};
const Color::Color Color::BLUESKY{0x90, 0x90, 0xE0, 0xFF};

std::ostream& operator<<(std::ostream& out, const SDL::Color::Color& color) {
    out << "Color{" << (int)color.r << ", " << (int)color.g << ", " << (int)color.b << ", " << (int)color.a << "}";
    return out;
}
