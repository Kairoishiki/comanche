#include "ttf.hpp"
#include "surface.hpp"
#include "utils.hpp"
#include <stdexcept>

TTF::Font::Font(const std::string& filename, int fontSize):
    m_filename(filename),
    m_fontSize(fontSize)
{
    TTF_Font *font = TTF_OpenFont(filename.c_str(), fontSize);
    if(font == nullptr){
        throw SDL::sdl_exception("Unable to open font");
    }
    m_font = ptr_t(font, TTF_CloseFont);
}

TTF_Font* TTF::Font::c_ptr() {
    return m_font.get();
}

std::pair<int, int> TTF::Font::sizeText(const std::string& text) const {
    int w, h;
    TTF_SizeText(m_font.get(), text.c_str(), &w, &h);
    return std::make_pair(w, h);
}

void TTF::FontManager::openFont(const std::string& filename, const std::string& name, int fontSize) {
    TTF::Font font(filename, fontSize);
    m_data.insert(std::make_pair(std::make_pair(name, fontSize), std::move(font)));
}

TTF::Font& TTF::FontManager::operator[](const TTF::FontManager::key_t& key) {
    return m_data.at(key);
}