# $@: nom de la cible
# $<: nom de la première dépendance
# $^: liste des dépendances
# $?: liste des dépendances plus récentes que la cible
# $*: nom d'un fichier sans son suffixe

CC = g++
CFLAGS = -W -Wall -O3 -std=c++17
LIBS =
LDFLAGS = `sdl2-config --cflags --libs` -lSDL2_ttf -lSDL2_mixer -lSDL2_image
INCLUDES =
EXEC = comanche
SRC = $(wildcard ./src/*.cpp) $(wildcard ./src/sdlwrapper/*.cpp) $(wildcard ./src/entities/*.cpp)
HEADER = $(wildcard ./src/*.hpp) $(wildcard ./src/sdlwrapper/*.hpp) $(wildcard ./src/entities/*.hpp)
OBJ = $(SRC:.cpp=.o)

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ $^ $(LIBS) $(LDFLAGS)

%.o: %.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -rf $(wildcard ./src/*.o) $(wildcard ./src/sdlwrapper/*.o) $(wildcard ./src/entities/*.o)

mrproper: clean
	rm -rf $(EXEC)

cloc:
	cloc $(SRC) $(HEADER)
