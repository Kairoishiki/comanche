

# Dependencies

-   gcc
-   make
-   SDL2
-   SDL2_image
-   SDL2_mixer
-   SDL2_ttf


# How to Play

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">keys</th>
<th scope="col" class="org-left">action</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Z</td>
<td class="org-left">forward</td>
</tr>


<tr>
<td class="org-left">S</td>
<td class="org-left">backward</td>
</tr>


<tr>
<td class="org-left">Q</td>
<td class="org-left">turn left</td>
</tr>


<tr>
<td class="org-left">D</td>
<td class="org-left">turn right</td>
</tr>


<tr>
<td class="org-left">A</td>
<td class="org-left">up</td>
</tr>


<tr>
<td class="org-left">E</td>
<td class="org-left">down</td>
</tr>


<tr>
<td class="org-left">F</td>
<td class="org-left">decrease the field of view</td>
</tr>


<tr>
<td class="org-left">R</td>
<td class="org-left">increase the field of view</td>
</tr>
</tbody>
</table>


# Credits

Inspired by "<https://github.com/s-macke/VoxelSpace/>"

